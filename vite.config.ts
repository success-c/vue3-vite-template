import {
  createStyleImportPlugin,
  ElementPlusResolve,
} from 'vite-plugin-style-import';
import { defineConfig, loadEnv } from 'vite';
import vue from '@vitejs/plugin-vue';
import { resolve } from 'path';
import AutoImport from 'unplugin-auto-import/vite';
import Components from 'unplugin-vue-components/vite';
import { createHtmlPlugin } from 'vite-plugin-html';
import viteCompression from 'vite-plugin-compression';
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers';

// https://vitejs.dev/config/
export default defineConfig({
  // base: ,
  plugins: [
    vue({
      include: [/\.vue$/, /\.md$/],
      // 可以使用$ref
      reactivityTransform: true,
    }),
    AutoImport({
      resolvers: [ElementPlusResolver()],
      dts: 'src/auto-imports.d.ts',
      include: [
        /\.[tj]sx?$/, // .ts, .tsx, .js, .jsx
        /\.vue$/,
        /\.vue\?vue/, // .vue
        /\.md$/, // .md
      ],
      vueTemplate: true,
      imports: [
        'vue',
        'vue-router',
        // custom
        {
          '@vueuse/core': [
            // named imports
            'useMouse', // import { useMouse } from '@vueuse/core',
            // alias
            ['useFetch', 'useMyFetch'], // import { useFetch as useMyFetch } from '@vueuse/core',
          ],
          axios: [
            // default imports
            ['default', 'axios'], // import { default as axios } from 'axios',
          ],
        },
      ],
    }),

    Components({
      // allow auto load markdown components under `./src/components/`
      extensions: ['vue', 'md'],
      // allow auto import and register components used in markdown
      include: [/\.vue$/, /\.tsx$/, /\.vue\?vue/, /\.md$/],
      resolvers: [ElementPlusResolver()],
      dts: 'src/components.d.ts',
    }),
    // 解决message和notification引入不生效问题
    createStyleImportPlugin({
      resolves: [ElementPlusResolve()],
    }),
    createHtmlPlugin({
      inject: {
        data: {
          title: 'vite+vue3+ts',
          description: '系统',
          version: '1.0.1',
        },
      },
    }),
    // 开启gzip压缩
    // viteCompression({
    //   ext: ".gz",
    //   algorithm: "gzip",
    //   deleteOriginFile: true,
    // }),
  ],

  resolve: {
    alias: {
      '@': resolve('./src'),
    },
  },

  css: {
    preprocessorOptions: {
      // less: {
      // 全局添加less
      // additionalData: `@import '@/assets/styles/common/var.less';`,
      // javascriptEnabled: true,
      // },
    },
  },
  build: {
    outDir: './dist',
    // 构建后是否生成 sourcemap 文件
    sourcemap: false,
    // 块大小警告大小限制(kb)
    chunkSizeWarningLimit: 1000,
    /** Vite 2.6.x 以上需要配置 minify: "terser", terserOptions 才能生效 */
    minify: 'terser',
    /** 在打包代码时移除 console.log、debugger 和 注释 */
    terserOptions: {
      //打包后移除console和注释
      compress: {
        drop_console: false,
        drop_debugger: true,
        pure_funcs: ['console.log'],
      },
      format: {
        /** 删除注释 */
        comments: false,
      },
    },
    /** 打包后静态资源目录 */
    assetsDir: 'static',
    rollupOptions: {
      output: {
        // 分解大块js,
        manualChunks(id) {
          if (id.includes('node_modules')) {
            return id
              .toString()
              .split('node_modules/')[1]
              .split('/')[0]
              .toString();
          }
        },
      },
    },
  },
  server: {
    port: 8001,
    proxy: {
      '/api': {
        target: 'http://1.116.40.155:9002/',
        ws: false,
        changeOrigin: true,
        rewrite: path => path.replace(/^\/api/, '/api/v1'),
      },
      '/resource': {
        target: 'https://static.fhtwl.cc',
        ws: false,
        changeOrigin: true,
        rewrite: path => path.replace(/^\/resource/, ''),
      },
    },
  },
});
