import { Directive } from "vue";
type Props = {
  text: string;
  color: string;
  size: number;
  font: string;
  align: CanvasTextAlign;
  el: any;
};
function addWaterMarker({
  text,
  color,
  size,
  align,
  font,
  el,
}: Partial<Props>) {
  let ctx = document.createElement("canvas");
  ctx.width = 200;
  ctx.height = 200;
  let cans = ctx.getContext("2d")!;
  cans?.rotate((-20 * Math.PI) / 180); // 旋转弧度
  cans.font = `${size}px ${font}` || "16px Microsoft JhengHei"; // 字体
  cans.fillStyle = color || "rgba(0,0,0,0.1)"; // 字体填充颜色
  cans.textAlign = align || "left"; // 对齐方式
  cans.textBaseline = "middle"; // 基线位置
  cans.fillText(text!, ctx.width / 3, ctx.height / 2);
  el.style.backgroundImage = `url(${ctx.toDataURL("image/png")})`; //插入背景图
}

const waterMarker: Directive<HTMLInputElement, Props> = (el, binding) => {
  addWaterMarker({ ...binding.value, el });
};

export default waterMarker;
