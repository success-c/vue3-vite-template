import { Directive } from "vue";

export const btnPermission: Directive<HTMLInputElement> = (el, binding) => {
  // 这里在mounted和update时都调用
  // 获取自定义指令内容
  const btnKey = binding.value;
  if (btnKey) {
    const key = checkKey(btnKey);
    if (!key) {
      el.remove();
    }
  }
};

const checkKey = (key: string) => {
  // 获取后端返回的按钮权限组
  const permissionBtnArr = JSON.parse(
    sessionStorage.getItem("permissionBtnArr")!
  );
  // 如果传入的元素key不在权限组中
  const permissionData = permissionBtnArr || [];
  const index = permissionData.indexOf(key);
  return index > -1;
};
