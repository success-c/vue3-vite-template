import "./style.css";
import App from "./App.vue";
import router from "./router";
import { createPinia } from "pinia";
import { ElMessage } from "element-plus";
import refreshRoute from "./utils/refreshRoute";
import { btnPermission } from "./directive/btnPermission";
import waterMark from "./directive/waterMark";
import "./utils/http";
import { registerStore } from "./store";
import loadElementIcon from "./plugins/loadElementIcon";
const app = createApp(App);

refreshRoute().then(() => {
  app.use(router);
  loadElementIcon(app);
  app.directive("permissions", btnPermission);
  app.directive("waterMark", waterMark);
  app.use(createPinia());
  registerStore();
  app.config.globalProperties.$message = ElMessage;
  app.mount("#app");
});
