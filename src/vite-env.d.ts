/// <reference types="vite/client" />
import { IMessage } from "element-plus/lib/el-message/src/types";

declare module "*.vue" {
  import type { DefineComponent } from "vue";
  const component: DefineComponent<{}, {}, any>;
  export default component;
}

declare module "js-base64";

//定义module 需要ts代码提示必须执行下方代码
declare module "@vue/runtime-core" {
  interface ComponentCustomProperties {
    $message: IMessage; //挂载类型
  }
}

declare module "nprogress";
