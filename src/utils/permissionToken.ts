import { userStore } from "@/store/useStore";
import { decode } from "js-base64";
export default () => {
  let jwt = sessionStorage.getItem("jwt") || "";
  if (jwt) {
    let token = null;
    try {
      token = JSON.parse(decode(jwt));
    } catch (e) {
      console.log("%c The user's JWT is error", "color:red");
    }
    !!token && userStore().setUser(token);
  }
};
