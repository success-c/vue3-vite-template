import { AxiosRequestConfig, AxiosResponse } from "axios";
import { encodeParam } from "./encrypt";
const router = useRouter();
const route = useRoute();
// 创建axios实例
const service = axios.create({
  baseURL: import.meta.env.VITE_APP_API_BASE_URL,
  timeout: 10000,
});

let loading = true;
let requestNum = 0;
let loadingInstance: any | null = null;

const addLoading = () => {
  // 增加loading 如果pending请求数量等于1，弹出loading, 防止重复弹出
  requestNum++;
  if (requestNum == 1) {
    loadingInstance = ElLoading.service({
      text: "正在努力加载中....",
      background: "rgba(0, 0, 0, 0)",
    });
  }
};

const cancelLoading = () => {
  // 取消loading 如果pending请求数量等于0，关闭loading
  requestNum--;
  if (requestNum === 0) loadingInstance?.close();
};

// 请求拦截
service.interceptors.request.use(
  (config: AxiosRequestConfig) => {
    // if (config.url !== "login") {
    //   const token = localStorage.getItem("token");
    //   config.headers!.Authorization = token;
    // }

    if (loading) {
      addLoading();
    }

    return config;
  },
  (err) => {
    if (loading) cancelLoading();
    return Promise.reject(err);
  }
);

// 响应拦截
service.interceptors.response.use(
  (response: AxiosResponse) => {
    if (loading) cancelLoading();
    const { code, message, data } = response.data;
    if (code == 200) return data;
    else if (code == 401 || code == 403) {
      router.replace(`/login?redirect=${encodeParam(route.fullPath)}`);
    } else {
      ElMessage.error(message || "Error");
      return Promise.reject(new Error(message || "Error"));
    }
  },
  (error) => {
    if (loading) cancelLoading();

    if (error.response) {
      if (error.response.status === 401) {
        router.replace(`/login?redirect=${encodeParam(route.fullPath)}`);
      }
    }
    ElMessage.error(error?.response?.data?.message || "服务端异常");
    return Promise.reject(error);
  }
);

class request {
  static get(url: string, paramObj: {} = {}): Promise<any> {
    return service.get(url, { params: paramObj });
  }
  static post(url: string, params: {} = {}): Promise<any> {
    return service.post(url, params);
  }
}

export default request;
