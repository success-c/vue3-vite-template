export interface UserModel {
  username?: string;
  pwd?: string;
  role?: string;
}
export interface Meta {
  title?: string;
  icon?: string;
  roles?: any[];
  redirect?: any;
  needCache?: boolean;
  fixed?: boolean;
}
export interface MenuProps {
  path?: string;
  name?: string;
  redirect?: any;
  hidden?: boolean;
  component?: any;
  meta?: Meta;
  children?: Array<MenuProps>;
}
