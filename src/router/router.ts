import Layout from "../layout/Index.vue";
import { MenuProps } from "@/model/userModel";
import { RouteRecordRaw } from "vue-router";

export const layoutMap: Array<RouteRecordRaw & MenuProps> = [
  {
    path: "/index",
    name: "Index",
    meta: { title: "工作台", icon: "HomeFilled", fixed: true },
    component: () => import("@/views/index/index.vue"),
  },
  {
    path: "/data",
    name: "Data",
    meta: { title: "数据管理", icon: "DataLine" },
    children: [
      {
        path: "",
        name: "DataList",
        meta: { title: "数据列表" },
        component: () => import("@/views/data/dataList/index.vue"),
      },
      {
        path: "table",
        name: "DataTable",
        meta: { title: "数据表格", roles: ["admin"] },
        component: () => import("@/views/data/dataTable/index.vue"),
      },
    ],
  },
  {
    path: "/admin",
    name: "Admin",
    meta: { title: "用户管理", icon: "User", roles: ["user"] },
    children: [
      {
        path: "",
        name: "AdminAuth",
        meta: { title: "用户列表" },
        component: () => import("."),
      },
      {
        path: "role",
        name: "AdminRole",
        meta: { title: "角色列表" },
        component: () => import("."),
      },
    ],
  },

  {
    path: "/user",
    name: "User",
    hidden: true /* 不在侧边导航展示 */,
    meta: { title: "个人中心" },
    component: () => import("."),
  },
  {
    path: "/error",
    name: "NotFound",
    hidden: true,
    meta: { title: "404" },
    component: () => import("."),
  },
  {
    path: "/:pathMatch(.*)*",
    hidden: true,
    redirect: { name: "NotFound" },
  },
];
export const routes: any = [
  {
    path: "/login",
    name: "Login",
    meta: { title: "登录", notNeedAuth: true },
    component: () => import("../views/login/Login.vue"),
  },
  {
    path: "/",
    redirect: "index",
    name: "Layout",
    component: Layout,
  },
];
