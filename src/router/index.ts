import { userStore } from "./../store/useStore/index";
import { tagsStore } from "@/store/tagsStore";
import {
  createRouter,
  createWebHistory,
  RouteLocationNormalized,
  _RouteRecordBase,
} from "vue-router";
import { layoutMap, routes } from "./router";
import NProgress from "nprogress";
import "nprogress/nprogress.css";
NProgress.configure({ showSpinner: false });

const router = createRouter({
  history: createWebHistory(),
  routes: [...routes],
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    } else {
      return { top: 0 };
    }
  },
});

let hasRoles = true;
router.beforeEach(async (to, form, next) => {
  NProgress.start();

  if (sessionStorage.getItem("jwt")) {
    if (to.path == "/login") {
      next({ path: "/" });
    } else {
      // 获取处理好的路由
      let routes = userStore().routers;
      // 路由添加进去了没有及时更新 需要重新进去一次拦截
      if (hasRoles) {
        routes.forEach((item) => router.addRoute("Layout", item));
        hasRoles = false;
        next({ ...to, replace: true }); // 这里相当于push到一个页面 不在进入路由拦截
      } else {
        next(); // 如果不传参数就会重新执行路由拦截，重新进到这里
      }
    }
  } else {
    if (to.path !== "/login") {
      router.replace(`/login`);
    }
    next();
  }
});

router.afterEach((to: RouteLocationNormalized): void => {
  NProgress.done();

  const tStore = tagsStore();
  // 添加缓存路由
  if (to.name && to.meta && to.meta.needCache) {
    tStore.addCacheView(to.name.toString());
  }
  const { name, path, meta, params, query } = to;
  if (to.meta && !to.meta.notNeedAuth) {
    tStore.addVisitedView({
      name,
      path,
      meta,
      params,
      query,
    } as _RouteRecordBase);
  }
});

export default router;
