# $ref 的弊端
# 应为他编译之后就是 count.value 并不是一个ref对象所以watch 无法监听而且会抛出一个警告

# [Vue warn]: Invalid watch source:  0 A watch source can only be a getter/effect function, a ref, a reactive object, or an array of these types. 
 

# 解决这个问题需要$$ 符号 就是再让他编译的时候变成一个ref 对象不加.value 
 
# watch($$(count),(v)=>{
#    console.log(v)
# })
 
# 在之前我们解构一个对象使用toRefs 解构完成之后 获取值和修改值 还是需要.value

# vue3 也提供了 语法糖  $() 解构完之后可以直接赋值

# const obj = reactive({name:'liu'})   let {name} = $(obj)